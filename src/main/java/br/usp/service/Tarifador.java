package br.usp.service;

import br.usp.model.Chamada;
import br.usp.model.DuracaoDeChamada;
import org.springframework.stereotype.Service;

/**
 * Created by mauriciourbanofilho on 15/03/16.
 */
@Service
public class Tarifador {

    protected static final Double TARIFA_PADRAO_POR_MINUTO = 0.40;
    protected static final Double DESCONTO_60_MINUTOS = 0.85;
    protected static final Integer UMA_HORA = 3600;

    /**
     * Realiza tarifação em chamadas telefônicas
     * @param chamada Chamada telefônica que deve ser tarifada
     * @return tarifa Total a ser pago pela chamada telefônica realizada
     */
    public Chamada tarifaChamada(Chamada chamada) {

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);

        Double tarifa = tarifaHorarioComercialComValorPadrao(duracaoDeChamada);
        tarifa = tarifa + tarifaHorarioNoturno(duracaoDeChamada);

        if (duracaoDeChamada.getSegundosComerciais() + duracaoDeChamada.getSegundosNoturnos() >= UMA_HORA) {
            tarifa = aplicaDesconto(tarifa, DESCONTO_60_MINUTOS);
        }
        chamada.setTarifa(tarifa);
        return chamada;
    }


    /**
     * Atribui o valor que deve ser cobrado pelo período em que a chamada esteve ativa no horário comercial
     * @return tarifa valor que deve ser cobrado pelo período em que a chamada esteve ativa no horário comercial
     */
    private Double tarifaHorarioComercialComValorPadrao(DuracaoDeChamada duracaoDeChamada) {
        Double tarifa = duracaoDeChamada.getSegundosComerciais() * getTarifaPadraPorSegundo();
        return tarifa;
    }

    /**
     * Atribui o valor que deve ser cobrado pelo período em que a chamada esteve ativa no horário noturno
     * @return tarifa valor que deve ser cobrado pelo período em que a chamada esteve ativa no horário noturno
     */
    private Double tarifaHorarioNoturno(DuracaoDeChamada duracaoDeChamada) {
        Double tarifa = duracaoDeChamada.getSegundosNoturnos() * getTarifaNoturnaPorSegundo();
        return tarifa;

    }


    /**
     * Com base na tarifa de horário comercial por minuto, calcula o valor da tarifa por segundo
     * @return tarifaPorSegundo tarifa por segundo comercial
     */
    private Double getTarifaPadraPorSegundo() {
        Double tarifaPorSegundo = TARIFA_PADRAO_POR_MINUTO / 60;
        return tarifaPorSegundo;
    }

    /**
     * Com base na tarifa de horário comercial por minuto, aplica o desconto necessário para gerar a tarifa por segundo noturno
     * @return tarifaPorSegundo tarifa por segundo noturno
     */
    private Double getTarifaNoturnaPorSegundo() {
        Double tarifaPorSegundo = (TARIFA_PADRAO_POR_MINUTO / 60);
        tarifaPorSegundo = aplicaDesconto(tarifaPorSegundo, 0.5);
        return tarifaPorSegundo;
    }

    /**
     * Aplica descontos a um determinado valor
     * @param valorOriginal valor para no qual deseja-se atribuir um desconto
     * @param desconto desconto que deseja-se conceder
     */
    private Double aplicaDesconto(Double valorOriginal, Double desconto) {
        return valorOriginal * desconto;
    }
}
