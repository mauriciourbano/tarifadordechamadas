package br.usp.service;

import br.usp.model.Chamada;
import br.usp.model.DuracaoDeChamada;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by mauriciourbanofilho on 16/03/16.
 *
 * Contém o motor de execução utilizado pelo Tarifador.
 *
 */
@Service
public class EngineTarifador {

    /**
     * @param chamada Chamada telefônica que deve ser analisada
     * @return DuracaoDeChamada Objeto com a quantidade de segundos comerciais e noturnos referentes à chamada analisada
     */
    public static DuracaoDeChamada calculaDuracaoDaChamada(Chamada chamada) {

        long segundosComerciais = 0;
        long segundosNoturnos = 0;

        DuracaoDeChamada duracaoDeChamada = new DuracaoDeChamada();

        while (chamada.getDataInicio().isBefore(chamada.getDataFim()) || chamada.getDataInicio().isEqual(chamada.getDataFim())) {

            LocalDateTime inicioHorarioComercial = getInicioHorarioComercial(chamada);
            LocalDateTime inicioHorarioNoturno = getInicioHorarioNoturno(chamada);

            if (iniciaETerminaNoHorarioComercial(chamada, inicioHorarioComercial, inicioHorarioNoturno)) {

                segundosComerciais = segundosComerciais + Duration.between(chamada.getDataInicio(), chamada.getDataFim()).getSeconds();

            } else if (iniciaAntesDoHorarioComercialETerminaNoHorarioComercial(chamada, inicioHorarioComercial, inicioHorarioNoturno)) {

                segundosComerciais = segundosComerciais + Duration.between(inicioHorarioComercial, chamada.getDataFim()).getSeconds();
                segundosNoturnos = segundosNoturnos + Duration.between(chamada.getDataInicio(), inicioHorarioComercial).getSeconds();

            } else if (iniciaNoHorarioNoturnoETerminaNoHorarioComercial(chamada, inicioHorarioComercial, inicioHorarioNoturno)) {

                segundosNoturnos = segundosNoturnos + Duration.between(chamada.getDataInicio(), inicioHorarioComercial.plusDays(1)).getSeconds();

            } else if (iniciaNoHorarioComercialETerminaNoHorarioNoturno(chamada, inicioHorarioComercial, inicioHorarioNoturno)) {

                segundosComerciais = segundosComerciais + Duration.between(chamada.getDataInicio(), inicioHorarioNoturno).getSeconds();
                segundosNoturnos = segundosNoturnos +  Duration.between(inicioHorarioNoturno, chamada.getDataFim()).getSeconds();

            } else if (iniciaETerminaNoHorarioNoturno(chamada, inicioHorarioComercial, inicioHorarioNoturno)) {

                segundosComerciais = segundosComerciais + Duration.between(inicioHorarioComercial, inicioHorarioNoturno).getSeconds();
                segundosNoturnos = segundosNoturnos +  Duration.between(chamada.getDataInicio(), inicioHorarioComercial).getSeconds();

                if (chamada.getDataInicio().getDayOfYear() == chamada.getDataFim().getDayOfYear() ||
                        chamada.getDataFim().isBefore(inicioHorarioComercial.plusDays(1))) {
                    segundosNoturnos = segundosNoturnos +  Duration.between(inicioHorarioNoturno, chamada.getDataFim()).getSeconds();
                } else {
                    segundosNoturnos = segundosNoturnos + Duration.between(inicioHorarioNoturno, inicioHorarioComercial.plusDays(1)).getSeconds();
                }
            }

            chamada.setDataInicio(chamada.getDataInicio().plusDays(1).withHour(8).withMinute(0).withSecond(0));
        }

        incrementaDuracaoDaChamada(duracaoDeChamada, segundosComerciais, segundosNoturnos);
        return duracaoDeChamada;
    }

    /**
     * @param chamada Chamada telefonica que será analisada
     * @param inicioHorarioComercial LocalDateTime com o início do horário comercial
     * @param inicioHorarioNoturno LocalDateTime com o início do horário noturno
     * @return boolean se chamada foi inicializada e finalizada no horário noturno
     */
    private static boolean iniciaETerminaNoHorarioNoturno(Chamada chamada, LocalDateTime inicioHorarioComercial, LocalDateTime inicioHorarioNoturno) {
        return chamada.getDataInicio().isBefore(inicioHorarioComercial) &&
                (chamada.getDataFim().isEqual(inicioHorarioNoturno) || chamada.getDataFim().isAfter(inicioHorarioNoturno));
    }

    /**
     * @param chamada Chamada telefonica que será analisada
     * @param inicioHorarioComercial LocalDateTime com o início do horário comercial
     * @param inicioHorarioNoturno LocalDateTime com o início do horário noturno
     * @return boolean se chamada foi inicializada no horário noturno e finalizada no horário comercial
     */
    private static boolean iniciaNoHorarioNoturnoETerminaNoHorarioComercial(Chamada chamada, LocalDateTime inicioHorarioComercial, LocalDateTime inicioHorarioNoturno) {
        return chamada.getDataInicio().isAfter(inicioHorarioNoturno) || (chamada.getDataInicio().isEqual(inicioHorarioNoturno)) &&
                (chamada.getDataFim().isAfter(inicioHorarioComercial.plusDays(1)) || chamada.getDataFim().isEqual(inicioHorarioComercial.plusDays(1)));
    }

    /**
     * @param chamada Chamada telefonica que será analisada
     * @param inicioHorarioComercial LocalDateTime com o início do horário comercial
     * @param inicioHorarioNoturno LocalDateTime com o início do horário noturno
     * @return boolean se chamada foi inicializada no horário comercial e finalizada no horário noturno
     */
    private static boolean iniciaNoHorarioComercialETerminaNoHorarioNoturno(Chamada chamada, LocalDateTime inicioHorarioComercial, LocalDateTime inicioHorarioNoturno) {
        return chamada.getDataInicio().isAfter(inicioHorarioComercial) || (chamada.getDataInicio().isEqual(inicioHorarioComercial)) &&
                (chamada.getDataFim().isAfter(inicioHorarioNoturno) || chamada.getDataFim().isEqual(inicioHorarioNoturno));
    }

    /**
     * @param chamada Chamada telefonica que será analisada
     * @param inicioHorarioComercial LocalDateTime com o início do horário comercial
     * @param inicioHorarioNoturno LocalDateTime com o início do horário noturno
     * @return boolean se chamada foi inicializada antes do horario comercial e finalizada no horário comercial
     */
    private static boolean iniciaAntesDoHorarioComercialETerminaNoHorarioComercial(Chamada chamada, LocalDateTime inicioHorarioComercial, LocalDateTime inicioHorarioNoturno) {
        return chamada.getDataInicio().isBefore(inicioHorarioComercial) && chamada.getDataFim().isBefore(inicioHorarioNoturno);
    }

    /**
     * @param chamada Chamada telefonica que será analisada
     * @param inicioHorarioComercial LocalDateTime com o início do horário comercial
     * @param inicioHorarioNoturno LocalDateTime com o início do horário noturno
     * @return boolean se chamada foi inicializada e finalizada no horário comercial
     */
    private static boolean iniciaETerminaNoHorarioComercial(Chamada chamada, LocalDateTime inicioHorarioComercial, LocalDateTime inicioHorarioNoturno) {
        return (chamada.getDataInicio().isAfter(inicioHorarioComercial) ||chamada.getDataInicio().isEqual(inicioHorarioComercial))
                        && chamada.getDataFim().isBefore(inicioHorarioNoturno);
    }

    /**
     * @param duracaoDeChamada Objeto que representa a duração da chamada telefônica
     * @param segundosComerciais tempo (segundos) que a chamada esteve ativa em horário comercial
     * @param segundosNoturnos tempo (segundos) que a chamada esteve ativa em horário noturno
     */
    private static void incrementaDuracaoDaChamada(DuracaoDeChamada duracaoDeChamada, long segundosComerciais, long segundosNoturnos) {
        duracaoDeChamada.setSegundosComerciais(segundosComerciais);
        duracaoDeChamada.setSegundosNoturnos(segundosNoturnos);
    }

    /**
     * @param chamada Chamada telefonica que será analisada
     * @return inicioHorarioComercial LocalDateTime com o início do horário comercial para a data da chamada analisada
     */
    private static LocalDateTime getInicioHorarioComercial(Chamada chamada) {
        LocalDateTime inicioHorarioComercial = LocalDateTime.of(
                chamada.getDataInicio().getYear(),
                chamada.getDataInicio().getMonth(),
                chamada.getDataInicio().getDayOfMonth(), 8, 0, 0);

        return inicioHorarioComercial;
    }

    /**
     * @param chamada Chamada telefonica que será analisada
     * @return inicioHorarioComercial LocalDateTime com o início do horário noturno para a data da chamada analisada
     */
    private static LocalDateTime getInicioHorarioNoturno(Chamada chamada) {
        LocalDateTime inicioHorarioNoturno = LocalDateTime.of(
                chamada.getDataInicio().getYear(),
                chamada.getDataInicio().getMonth(),
                chamada.getDataInicio().getDayOfMonth(), 18, 0, 0);

        return inicioHorarioNoturno;
    }
}
