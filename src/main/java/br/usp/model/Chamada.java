package br.usp.model;

import java.time.LocalDateTime;

/**
 * Created by mauriciourbanofilho on 15/03/16.
 *
 * Representa uma chamada telefônica
 */
public class Chamada {

    private LocalDateTime dataInicio;
    private LocalDateTime dataFim;
    private Double tarifa;


    public Chamada() {
        dataInicio = LocalDateTime.now();
    }

    public LocalDateTime getDataInicio() {
        return dataInicio;
    }

    public LocalDateTime getDataFim() {
        return dataFim;
    }

    public void setDataFim(LocalDateTime dataFim) {
        this.dataFim = dataFim;
    }

    public void setDataInicio(LocalDateTime dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Double getTarifa() {
        return tarifa;
    }

    public void setTarifa(Double tarifa) {
        this.tarifa = tarifa;
    }
}
