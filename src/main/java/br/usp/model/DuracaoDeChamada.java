package br.usp.model;

/**
 * Created by mauriciourbanofilho on 17/03/16.
 *
 * Representa a duração de uma chamada telefônica
 */
public class DuracaoDeChamada {

    private long segundosComerciais;
    private long segundosNoturnos;

    public long getSegundosComerciais() {
        return segundosComerciais;
    }

    public void setSegundosComerciais(long segundosComerciais) {
        this.segundosComerciais = segundosComerciais;
    }

    public long getSegundosNoturnos() {
        return segundosNoturnos;
    }

    public void setSegundosNoturnos(long segundosNoturnos) {
        this.segundosNoturnos = segundosNoturnos;
    }
}
