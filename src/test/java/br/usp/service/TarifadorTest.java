package br.usp.service;

import br.usp.Atividade2Application;
import br.usp.model.Chamada;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.Month;

/**
 * Created by mauriciourbanofilho on 15/03/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Atividade2Application.class)
public class TarifadorTest {

    Chamada chamada;

    @Before
    public void setup(){
        chamada = new Chamada();
    }


    /**
     * Início e término no horário comercial <br>
     * Deve cobrar apenas o valor R$ 0,40 por minuto. <br>
     * Início: 8h00 <br>
     * Término: 8h10 <br>
     * Duração: 0:10 <br>
     * Valor: R$ 4,00
     */
    @Test
    public void deveCobrar4ReaisTarifaPadraoEntreOitoInclusiveEDezoitoHoras() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 10, 0));

        Tarifador tarifador = new Tarifador();
        Double tarifa = tarifador.tarifaChamada(chamada).getTarifa();

        Assert.assertEquals(4, tarifa, 0.01);
    }

    /**
     * Início no horário comercial e término no horário noturno <br>
     * Deve cobrar o valor R$ 0,40 por minuto comercial e R$ 0,20 por minuto noturno. <br>
     * Início: 17h50 <br>
     * Término: 18h10 <br>
     * Duração: 0:20 <br>
     * Valor: R$ 6,00
     */
    @Test
    public void deveCobrar6ReaisTarifaPadraoENoturna() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 17, 50, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 1, 18, 10, 0));

        Tarifador tarifador = new Tarifador();
        Double tarifa = tarifador.tarifaChamada(chamada).getTarifa();

        Assert.assertEquals(6, tarifa, 0.01);
    }

    /**
     * Início e término no horário noturno <br>
     * Deve cobrar o valor R$ 0,20 por minuto e aplicar desconto de 15%. <br>
     * Início: 18:00 <br>
     * Término: 7:59:59 <br>
     * Duração: 13:59:59 <br>
     * Aplica desconto de 15% <br>
     * Valor completo: R$ 167,99 <br>
     * Valor com desconto: R$ 142,79
     */
    @Test
    public void deveCobrar13horas59MinutosE59SegundosTarifaNoturnaComDescontoQuinzePorCento() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 18, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 2, 7, 59, 59));

        Tarifador tarifador = new Tarifador();
        Double tarifa = tarifador.tarifaChamada(chamada).getTarifa();

        Assert.assertEquals(142.79, tarifa, 0.01);
    }

    /**
     * Início no horário noturno e término no horário comercial <br>
     * Deve cobrar o valor R$ 0,20 por minuto noturno e aplicar desconto de 15%. <br>
     * Início: 18:00 <br>
     * Término: 8:00:00 <br>
     * Duração: 14:00:00 <br>
     * Aplica desconto de 15% <br>
     * Valor completo: R$ 168 <br>
     * Valor com desconto: R$ 142,79
     */
    @Test
    public void deveCobrar14horasTarifaNoturnaEComercialComDescontoQuinzePorCento() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 18, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 2, 8, 0, 0));

        Tarifador tarifador = new Tarifador();
        Double tarifa = tarifador.tarifaChamada(chamada).getTarifa();

        Assert.assertEquals(142.7, tarifa, 0.1);
    }

    /**
     * Início no horário noturno e término no horário comercial <br>
     * Deve cobrar o valor R$ 0,20 por minuto noturno, R$ 0,40 por segundo comercial e aplicar desconto de 15%. <br>
     * Início: 18:00 <br>
     * Término: 8:01:00 <br>
     * Duração: 14:00:00 <br>
     * Aplica desconto de 15% <br>
     * Valor completo: R$ 168 <br>
     * Valor com desconto: R$ 142,79
     */
    @Test
    public void deveCobrar14horasE1MinutoTarifaNoturnaEComercialComDescontoQuinzePorCento() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 18, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 2, 8, 1, 0));

        Tarifador tarifador = new Tarifador();
        Double tarifa = tarifador.tarifaChamada(chamada).getTarifa();

        Assert.assertEquals(143.14, tarifa, 0.01);
    }

    /**
     * Início no horário comercial e término no horário noturno <br>
     * Deve cobrar o valor R$ 0,20 por minuto noturno, R$ 0,40 por segundo comercial e aplicar desconto de 15%. <br>
     * Início: 8:00 <br>
     * Término: 18:01:00 <br>
     * Duração: 10:01:00 <br>
     * Aplica desconto de 15% <br>
     * Valor completo: R$ 240,20 <br>
     * Valor com desconto: R$ 204,17
     */
    @Test
    public void deveCobrar12horasE1MinutoTarifaNoturnaEComercialComDescontoQuinzePorCento() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 1, 18, 1, 0));

        Tarifador tarifador = new Tarifador();
        Double tarifa = tarifador.tarifaChamada(chamada).getTarifa();

        Assert.assertEquals(204.17, tarifa, 0.01);
    }

    /**
     * Início e término no horário comercial <br>
     * Deve cobrar o valor R$ 0,40 por minuto noturno e aplicar desconto de 15%. <br>
     * Início: 8:00 <br>
     * Término: 9:00:00 <br>
     * Duração: 1:01:00 <br>
     * Aplica desconto de 15% <br>
     * Valor completo: R$ 24,00 <br>
     * Valor com desconto: R$ 20,40
     */
    @Test
    public void deveConcederDescontoDe15PorCento() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 1, 9, 0, 0));

        Tarifador tarifador = new Tarifador();
        Double tarifa = tarifador.tarifaChamada(chamada).getTarifa();

        Assert.assertEquals(20.4, tarifa, 0.01);
    }

    /**
     * Início e término no horário comercial <br>
     * Deve cobrar o valor R$ 0,40 por minuto noturno e não aplicar desconto de 15%. <br>
     * Início: 8:00 <br>
     * Término: 9:00:00 <br>
     * Duração: 1:01:00 <br>
     * Aplica desconto de 15% <br>
     * Valor completo: R$ 23,99
     */
    @Test
    public void naoDeveConcederDescontoDe15PorCento() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 59, 59));

        Tarifador tarifador = new Tarifador();
        Double tarifa = tarifador.tarifaChamada(chamada).getTarifa();

        Assert.assertEquals(23.99, tarifa, 0.01);
    }
}
