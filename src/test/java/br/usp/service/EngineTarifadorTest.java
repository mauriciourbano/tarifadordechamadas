package br.usp.service;

import br.usp.model.Chamada;
import br.usp.model.DuracaoDeChamada;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;

/**
 * Created by mauriciourbanofilho on 16/03/16.
 */
public class EngineTarifadorTest {

    Chamada chamada;

    @Before
    public void setup(){
        chamada = new Chamada();
    }

    @Test
    public void deveCalcular600SegundosComerciaisComInicioETerminoDentroHorarioComercial() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 10, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosComerciais = duracaoDeChamada.getSegundosComerciais();

        Assert.assertEquals(600, segundosComerciais);
    }

    @Test
    public void deveCalcular600SegundosNoturnosComInicioETerminoDentroHorarioNoturno() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.APRIL, 1, 18, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.APRIL, 1, 18, 10, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosNoturnos = duracaoDeChamada.getSegundosNoturnos();

        Assert.assertEquals(600, segundosNoturnos);
    }

    @Test
    public void deveCalcular600SegundosComerciaisComInicioEmUmDiaETerminoEmOutro() {
        chamada.setDataInicio(LocalDateTime.of(2015, Month.DECEMBER, 31, 17, 55, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 5, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosComerciais = duracaoDeChamada.getSegundosComerciais();

        Assert.assertEquals(600, segundosComerciais);
    }

    @Test
    public void deveCalcular600SegundosNoturnosComInicioAntesDas8hETerminoDepoisDas18h() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 7, 55, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 1, 18, 5, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosNoturnos = duracaoDeChamada.getSegundosNoturnos();

        Assert.assertEquals(600, segundosNoturnos);
    }

    @Test
    public void deveCalcular51000SegundosNoturnosComInicioNaManhaDoDia1ETerminoNoFimDoDia2() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 7, 55, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 2, 18, 5, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosNoturnos = duracaoDeChamada.getSegundosNoturnos();

        Assert.assertEquals(51000, segundosNoturnos);
    }

    @Test
    public void deveCalcular36600SegundosComerciaisComDuracaoDeDoisDias() {
        chamada.setDataInicio(LocalDateTime.of(2016, Month.JANUARY, 1, 17, 55, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.JANUARY, 3, 8, 5, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosComerciais = duracaoDeChamada.getSegundosComerciais();

        Assert.assertEquals(36600, segundosComerciais);
    }

    @Test
    public void deveCalcular600SegundosComerciaisComInicioNoturnoETerminoNoHorarioComercial() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.APRIL, 1, 7, 55, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.APRIL, 1, 8, 10, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosComerciais = duracaoDeChamada.getSegundosComerciais();

        Assert.assertEquals(600, segundosComerciais);
    }

    @Test
    public void deveCalcular600SegundosComerciaisComInicioComercialETerminoNoturno() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.APRIL, 1, 17, 50, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.APRIL, 1, 18, 10, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosComerciais = duracaoDeChamada.getSegundosComerciais();

        Assert.assertEquals(600, segundosComerciais);
    }

    @Test
    public void deveCalcular36000SegundosComerciais() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.APRIL, 1, 8, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.APRIL, 1, 18, 0, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosComerciais = duracaoDeChamada.getSegundosComerciais();

        Assert.assertEquals(36000, segundosComerciais);
    }

    @Test
    public void deveCalcular35999SegundosComerciais() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.APRIL, 1, 8, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.APRIL, 1, 17, 59, 59));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosComerciais = duracaoDeChamada.getSegundosComerciais();

        Assert.assertEquals(35999, segundosComerciais);
    }

    @Test
    public void deveCalcular50400SegundosNoturnos() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.APRIL, 1, 18, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.APRIL, 2, 8, 00, 00));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosNoturnos = duracaoDeChamada.getSegundosNoturnos();

        Assert.assertEquals(50400, segundosNoturnos);
    }

    @Test
    public void deveCalcular53399SegundosNoturnos() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.APRIL, 1, 18, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.APRIL, 2, 7, 59, 59));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosNoturnos = duracaoDeChamada.getSegundosNoturnos();

        Assert.assertEquals(50399, segundosNoturnos);
    }

    @Test
    public void deveCalcular60SegundosComercialE50400SegundosNoturnos() {

        chamada.setDataInicio(LocalDateTime.of(2016, Month.APRIL, 1, 18, 0, 0));
        chamada.setDataFim(LocalDateTime.of(2016, Month.APRIL, 2, 8, 1, 0));

        DuracaoDeChamada duracaoDeChamada = EngineTarifador.calculaDuracaoDaChamada(chamada);
        long segundosNoturnos = duracaoDeChamada.getSegundosNoturnos();
        long segundosComerciais = duracaoDeChamada.getSegundosComerciais();

        Assert.assertEquals(50400, segundosNoturnos);
        Assert.assertEquals(60, segundosComerciais);
    }
}
